# -*- coding: utf-8 -*-
"""
Created on Fri Jan 28 22:12:15 2022

@author: owen
"""
from pathlib import Path
import cv2
from tkinter import Label, Tk
import numpy as np
from PIL import ImageTk, Image

import sys

sys.path.insert(0,r'C:\Users\17742\Documents\DeepLearning\delta_dev\delta')

import delta
import delta.utilities as utils

imgpath = Path('../test_file')
pklpath = Path('../test_file/Position000000.pkl')



root = Tk()
root.title('Image')
root.geometry("450x300")

class gui():

    def __init__(
        self,
        imgpath: str,
        pklpath: str,
        ):
        
        self.imgpath = imgpath
        self.pklpath = pklpath 
        
        # Read pickle file
        self.reader = delta.utilities.xpreader(self.imgpath)

        # Init a the Pipeline class :
        self.proc = delta.pipeline.Pipeline(self.reader,reload=True)
        self.pos_nbs = [i for i in range(self.reader.positions)] #Initialize at position 0
        self.pos_nb = 0
        self.roi_nb = 0
        self.update_pos_roi()
        
        # Press n to initiate self.create_new_cell which will create a "new" cell with no previous lineage
        self.create_new_cell = False
        
        # A list which stores available cell numbers that can be recycled
        # Cell numbers get reused
        self.list_cellnbs_recycle = []

        remove = ['id','mother']
        self.features = [i for i in self.roi.lineage.cells[0].keys() if i not in remove]
                  
        # self.click_cell_prevframe == True --> looking at previous frame
        # self.click_cell_prevframe == False --> looking at the current frame
        self.click_cell_prevframe = True
        
        print('Click on a cell in frame t-1')
                
        # Display the first image (fnb is short for frame number)
        self.fnb = 1   # self.fnb is current frame and self.fnb-1 is the preivous frame
        self.init_image()
        self.display_images()
        
        # Create functionality from the keyboard to navigate left, right,
        root.bind('<Key>', self.key_press)
        root.bind('<Left>', self.leftKey)
        root.bind('<Right>', self.rightKey)
        root.bind('<Key>', self.key_press)
        root.bind('<Up>', self.upKey)
        root.bind('<Down>', self.downKey)
        root.bind("<Button 1>", self.getorigin)
        root.title('Live / Dead GUI')

        root.mainloop()

    def update_pos_roi(self):
        '''
        Based on the ROI nb nad Position nb, update the img, seg and label stack

        '''
        # Update position
        self.pos = self.proc.positions[self.pos_nb]
        
        self.roi_nbs = len(self.proc.positions)
        self.roi = self.pos.rois[self.roi_nb]
        self.lin = self.roi.lineage

        self.label_stack = self.roi.label_stack
        self.img_stack = self.roi.img_stack
        self.seg_stack = self.roi.seg_stack        
        self.numframes = len(self.seg_stack)  

    def init_image(self):
        '''
        Loads the images

        '''
        self.colors = utils.getrandomcolors(len(self.roi.lineage.cells)+1, seed=0)
        
        prev_lbl = self.label_stack[self.fnb - 1]
        cur_lbl = self.label_stack[self.fnb]
        
        prev_img = self.img_stack[self.fnb - 1]
        cur_img = self.img_stack[self.fnb]
        
        prev_color_img = np.repeat(prev_img[:,:,np.newaxis],repeats=3,axis=-1)
        cur_color_img = np.repeat(cur_img[:,:,np.newaxis],repeats=3,axis=-1)
        
        self.prev_color_img = self.color_cells(
            frame = prev_color_img,
            lbl_stack = prev_lbl,
            fnb = self.fnb - 1,
            )
        
        self.cur_color_img = self.color_cells(
            frame = cur_color_img,
            lbl_stack = cur_lbl,
            fnb = self.fnb,
            )
        
    def color_cells(
        self,
        frame,
        lbl_stack,fnb
        ):    
    
        cells, contours = utils.getcellsinframe(lbl_stack, return_contours=True)
        assert isinstance(cells, list)  # needed for mypy on Python < 3.8

        if self.roi.box is None:
            xtl, ytl = (0, 0)
        else:
            xtl, ytl = (self.roi.box["xtl"], self.roi.box["ytl"])
            
        # Run through cells in labelled frame:
        for c, cell in enumerate(cells):

            # Draw contours:
            frame = cv2.drawContours(
                frame,
                contours,
                c,
                color=self.colors[cell],
                thickness=1,
                offset=(xtl, ytl),
            )

            # Draw poles:
            oldpole = self.roi.lineage.getvalue(cell, fnb, "old_pole")
            assert isinstance(oldpole, np.ndarray)  # for mypy
            frame = cv2.drawMarker(
                frame,
                (oldpole[1] + xtl, oldpole[0] + ytl),
                color=self.colors[cell],
                markerType=cv2.MARKER_TILTED_CROSS,
                markerSize=3,
                thickness=1,
            )
    
            daughter = self.roi.lineage.getvalue(cell, fnb, "daughters")
            bornago = self.roi.lineage.cells[cell]["frames"].index(fnb)
            mother = self.roi.lineage.cells[cell]["mother"]
    
            if daughter is None and (bornago > 0 or mother is None):
                newpole = self.roi.lineage.getvalue(cell, fnb, "new_pole")
                frame = cv2.drawMarker(
                    frame,
                    (newpole[1] + xtl, newpole[0] + ytl),
                    color=[1, 1, 1],
                    markerType=cv2.MARKER_TILTED_CROSS,
                    markerSize=3,
                    thickness=1,
                )
    
            # Plot division arrow:
            if daughter is not None:
    
                newpole = self.roi.lineage.getvalue(cell, fnb, "new_pole")
                daupole = self.roi.lineage.getvalue(daughter, fnb, "new_pole")
                # Plot arrow:
                frame = cv2.arrowedLine(
                    frame,
                    (newpole[1] + xtl, newpole[0] + ytl),
                    (daupole[1] + xtl, daupole[0] + ytl),
                    color=(1, 1, 1),
                    thickness=1,
                )
                
        return frame * 255

            
    def display_images(self):
        '''
        Displays the current image / segmentation in the GUI

        '''
        
        text_prevframe = Label(root, text=f'Pos {self.pos_nb} ROI {self.roi_nb} Frame {self.fnb-1}')
        text_prevframe.config(font=('Courier',15))
        text_prevframe.grid(row=1,column=1)

        text_curframe = Label(root, text=f'Pos {self.pos_nb} ROI {self.roi_nb} Frame {self.fnb}')
        text_curframe.config(font=('Courier',15))
        text_curframe.grid(row=1,column=2)
        
        self.outline_selected_frame()

        PIL_prev_color_img = Image.fromarray(np.uint8(self.prev_color_img))
        previmg_PIL = ImageTk.PhotoImage(PIL_prev_color_img)
        self.my_label = Label(image=previmg_PIL)
        self.my_label.photo = previmg_PIL
        self.my_label.grid(row=2, column=1)  

        PIL_cur_color_img = Image.fromarray(np.uint8(self.cur_color_img))
        curimg_PIL = ImageTk.PhotoImage(PIL_cur_color_img)
        self.my_label = Label(image=curimg_PIL)
        self.my_label.photo = curimg_PIL
        self.my_label.grid(row=2, column=2)     
        
    def outline_selected_frame(self):
        '''
        Tells you which frame to select a cell from
        It can be difficult to click on a cell so this visually tells you when you clicked on a cell
        '''
        green = (0,255,0)
        red = (255,0,0)
        thickness = 3
        img_shape = self.prev_color_img.shape
        
        if self.click_cell_prevframe: # Highlight previous frame green because you're selecting a cell to track
            self.prev_color_img = cv2.rectangle(
                self.prev_color_img,
                (0, 0),
                (img_shape[1], img_shape[0]),
                color = green,
                thickness = thickness
                )
        else: # Click on cell in the current frame
            if self.create_new_cell: # highlight current frame red becase you're creating a new cell
                self.cur_color_img = cv2.rectangle(
                    self.cur_color_img,
                    (0, 0),
                    (img_shape[1], img_shape[0]),
                    color = red,
                    thickness = thickness,
                    )                   
            else: # highlight current frame green because you're tracking a cell from previou frame
                self.cur_color_img = cv2.rectangle(
                    self.cur_color_img,
                    (0, 0),
                    (img_shape[1], img_shape[0]),
                    color = green,
                    thickness = thickness,
                    )        
        
    def leftKey(self, event):
        '''
        Pressing the left key will look at the previous image''

        '''
        if self.fnb == 1:
            pass
        else:
            self.my_label.config(image='')
            self.click_cell_prevframe=True
            self.fnb -= 1
            self.init_image()
            self.display_images()

    def rightKey(self, event):
        '''
        Pressing the right key will look at the next image''

        '''
        if self.fnb == len(self.label_stack) - 1:
            pass
        else:
            self.my_label.config(image='')
            self.click_cell_prevframe=True
            self.fnb += 1
            self.init_image()
            self.display_images()
            
    def upKey(self, event):
        '''
        Pressing the up key will look at the next image''

        '''
        if self.fnb + 10 < self.numframes:
            self.fnb += 10
        else:
            self.fnb = self.numframes - 1
        self.my_label.config(image='')
        self.click_cell_prevframe=True
        self.init_image()
        self.display_images()

    def downKey(self, event):
        '''
        Pressing the left key will look at the previous image''

        '''
        if self.fnb - 10 > 1:
            self.fnb -= 10
        else:
            self.fnb = 1
        self.my_label.config(image='')
        self.click_cell_prevframe=True
        self.init_image()
        self.display_images()
            
    def getorigin(self, eventorigin):
        '''
        Gets the location of the cursor.

        Parameters
        ----------
        eventorigin : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        '''
        # Gets location of cursor
        x = int(eventorigin.x)
        y = int(eventorigin.y)

        # the key "n" initiates self.create_new_cell which will remove the cell's preivous lineage
        if self.create_new_cell: 
        
            # Check that a cell was clicked on
            if self.label_stack[self.fnb][y,x] != 0:
            
                self.curcellnb = self.label_stack[self.fnb][y,x] - 1
                index_frame_curcell = self.roi.lineage.cells[self.curcellnb]['frames'].index(self.fnb)                        
                
                # If cell's lineage starts at self.fnb then we do not need to create a new cell. 
                # We just have to remove the mother / daughter connection
                if self.roi.lineage.cells[self.curcellnb]['frames'].index(self.fnb) == 0:
                    
                    if not self.roi.lineage.cells[self.curcellnb]['mother']:
                        print('Cell already has no previous lineage')
                        
                    else:
                        self.mothercellnb = self.roi.lineage.cells[self.curcellnb]['mother']
                        self.roi.lineage.cells[self.curcellnb]['mother'] = None
                        self.roi.lineage.cells[self.mothercellnb]['daughters'][self.fnb] = None
                else:
                    
                    
                    if len(self.list_cellnbs_recycle) == 0:
                        self.roi.lineage.createcell(
                            frame = [],
                            daughters = [],
                            old_pole = [],
                            new_pole = [],
                            mother=None,
                            add_features = True
                            )
                        
                        self.newcellnb = self.roi.lineage.cells[-1]['id'] 
                    else:
                        self.newcellnb = self.list_cellnbs_recycle.pop()
                        self.roi.lineage.cells[self.newcellnb]['mother'] = None
                        self.roi.lineage.cells[self.newcellnb]['id'] = self.newcellnb                                    
        
                    for feature in self.features:
                       # Remove incorrect information from curcellnb
                       curcellnb_curinfo = self.roi.lineage.cells[self.curcellnb][feature][index_frame_curcell:]
                       curcellnb_previnfo = self.roi.lineage.cells[self.curcellnb][feature][:index_frame_curcell]
                  
                       self.roi.lineage.cells[self.newcellnb][feature] = curcellnb_curinfo  
                       self.roi.lineage.cells[self.curcellnb][feature] = curcellnb_previnfo
                               
                    if self.roi.lineage.cells[self.newcellnb]['mother']:
                        mothercellnb = self.roi.lineage.cells[self.newcellnb]['mother']
                        
                        if self.roi.lineage.cells[mothercellnb]['daughters'][self.fnb] == self.newcellnb:
                            
                            self.roi.lineage.cells[mothercellnb]['daughters'][self.fnb] = None
                        
                    for fnb in range(self.fnb,self.numframes):
                        self.label_stack[fnb][self.label_stack[fnb] == self.curcellnb + 1] = self.newcellnb + 1
                             
                self.create_new_cell = False
                self.click_cell_prevframe = True
                            
            else:
                
                print('Please pick a cell')
                
        # click on cell in previous frame
        elif self.click_cell_prevframe == True:
            
            if y > self.label_stack[0].shape[0] or x > self.label_stack[0].shape[1]:
                print('PLease click on a cell inside the frame')
                
            else:
                self.mothercellnb = self.label_stack[self.fnb - 1][y,x] - 1
                   
                # Check that you clicked on a cell
                if self.mothercellnb == -1:
                    print('You missed the cell')
                  
                # Check if mother cell is already dividing into two cells
                # GUI is set up so you can't track a cell in the previous frame to three cells in the current frame
             
                # First check if mother cell exists in current frame / tracks to at least one cell
                elif self.fnb in self.roi.lineage.cells[self.mothercellnb]['frames']:
                    # Get frame index of mother cell
                    fn_idx = self.roi.lineage.cells[self.mothercellnb]['frames'].index(self.fnb)
                    
                    if self.roi.lineage.cells[self.mothercellnb]['daughters'][fn_idx]: # division
                        print(f'Cell {self.mothercellnb} already divides into 2 daughters cells ({self.mothercellnb} and {self.roi.lineage.cells[self.mothercellnb]["daughters"][self.fnb]})')
                        print('You need to remove one of the divisions first')
                    else: # no division
                        self.click_cell_prevframe=False
                        print('Click on a cell in frame t')                        
                else:
                    self.click_cell_prevframe=False
                    print('Click on a cell in frame t')

        # Click on cell in the current frame
        else:
            # Check that you clicked on a pixel within the frame
            if y > self.label_stack[0].shape[0] and x > self.label_stack[0].shape[1]:
                print('PLease click on a cell inside the frame')
            
            else:
                self.curcellnb = self.label_stack[self.fnb][y,x] - 1
                
                # Check if you clicked on background
                if self.curcellnb == -1:
                    print('You missed the cell')
                    
                # Check if you clicked on a cell that the mother cell already tracks to (no division)
                elif self.curcellnb == self.mothercellnb:
                    print(f'Cell {self.mothercellnb} already tracks to cell {self.curcellnb}')
    
                # Check if you clicked on a cell that the mother cell already tracks to (division)
                elif self.fnb in self.roi.lineage.cells[self.mothercellnb]['frames'] and  self.roi.lineage.cells[self.mothercellnb]['daughters'][self.roi.lineage.cells[self.mothercellnb]['frames'].index(self.fnb)] == self.curcellnb:
                    print(f'Cell number {self.mothercellnb} already tracks to daughter cell ({self.roi.lineage.cells[self.mothercellnb]["daughters"][self.fnb]})')
                    self.click_cell_prevframe = True

                # You clicked on a potential cell to track to
                else:
                    # Initialize a newcellnb for when 
                    self.newcellnb = None                
    
                    # Initialize a list of cells to update when iterating through lineage
                    self.cell_list_to_update = [self.mothercellnb]            
                    
                    # Get current frame index of current cell 
                    index_frame_curcell = self.roi.lineage.cells[self.curcellnb]['frames'].index(self.fnb)                        

                    # Check if the cell of interest is already tracking to a cell in the current frame
                    if self.fnb in self.roi.lineage.cells[self.mothercellnb]['frames']:
                      
                        # Get curent frame index of mother cell 
                        index_frame_mothercell = self.roi.lineage.cells[self.mothercellnb]['frames'].index(self.fnb)
                        
                        # Identify mother and daughter poles:
                        mother_poles, daughter_poles, first_cell_is_mother = self.check_poles(self.mothercellnb,self.curcellnb,self.fnb)     
    
                        # This checks if the mother cell remains the mother cell
                        if first_cell_is_mother:
                                               
                            ####
                            
                            # If curcell was already dividing into two cells
                            # lineage needs to be shifted to cell that hasn't already divided
                            
                            ####
                            
                            # If cell of interest in current frame exists in previous frame,
                            # you need to create a new cell
                            if self.fnb-1 in self.roi.lineage.cells[self.curcellnb]['frames']:
                                # Create new cell
                                if len(self.list_cellnbs_recycle) == 0:
                                    self.roi.lineage.createcell(
                                        frame = [],
                                        daughters = [],
                                        old_pole = [],
                                        new_pole = [],
                                        mother=self.mothercellnb,
                                        add_features = True
                                        )
                                    
                                    self.newcellnb = self.roi.lineage.cells[-1]['id'] 
                                else:
                                    self.newcellnb = self.list_cellnbs_recycle.pop()
                                    self.roi.lineage.cells[self.newcellnb]['mother'] = self.mothercellnb
                                    self.roi.lineage.cells[self.newcellnb]['id'] = self.newcellnb                                    
                   
                                index_frame_curcell = self.roi.lineage.cells[self.curcellnb]['frames'].index(self.fnb)
                                daucellnb = self.roi.lineage.cells[self.curcellnb]['daughters'][index_frame_curcell] 
                                
                                # Swap lineage information from curcell to the newcell for each feature
                                for feature in self.features:
                                   curcell_curinfo = self.roi.lineage.cells[self.curcellnb][feature][index_frame_curcell:]
                                   curcell_previnfo = self.roi.lineage.cells[self.curcellnb][feature][:index_frame_curcell]
                                   
                                   # Remove all future lineage information about curcellnb
                                   self.roi.lineage.cells[self.curcellnb][feature] = curcell_previnfo                             
                                 
                                   if daucellnb:
                                       # Swap daughter lineage info to curcell if curcell had a division
                                       daucell_curinfo = self.roi.lineage.cells[daucellnb][feature]
                                       self.roi.lineage.cells[self.curcellnb][feature].extend(daucell_curinfo)
                                       
                                   # Add info from curcellnb to newcellnb
                                   self.roi.lineage.cells[self.newcellnb][feature].extend(curcell_curinfo)

                                if daucellnb:
                                    self.roi.lineage.cells[daucellnb]['id'] = -1
                                    self.list_cellnbs_recycle.append(daucellnb)
                            
                                self.roi.lineage.setvalue(self.mothercellnb,self.fnb,'daughters',self.newcellnb)
                                
                                self.cell_list_to_update.append(self.newcellnb)
                                self.cell_list_to_update.append(self.curcellnb)
                                
                                # Update label_stack for rest of frames
                                for fnb in range(self.fnb,self.numframes):
                                    self.label_stack[fnb][self.label_stack[fnb] == self.curcellnb + 1] = self.newcellnb + 1
                                
                           # curcell has no previous lineage
                            else:
                                self.roi.lineage.cells[self.curcellnb]['mother'] = self.mothercellnb
                                self.roi.lineage.setvalue(self.mothercellnb,self.fnb,'daughters',self.curcellnb)
                                
                                self.cell_list_to_update.append(self.curcellnb)
                                                  
                                
                        # Else the mother and daugher cells are swapped in the current frame
                        else:
                                
                            # Need to check if curcell exists in the previous frame
                            # If so, then a new cell needs to be created
                            if self.fnb-1 in self.roi.lineage.cells[self.curcellnb]['frames']:
    
                                if len(self.list_cellnbs_recycle) == 0:
                                    # Create new cell
                                    self.roi.lineage.createcell(
                                        frame = [],
                                        daughters = [],
                                        old_pole = [],
                                        new_pole = [],
                                        mother=self.mothercellnb,
                                        add_features = True,
                                        )             
                                    
                                    self.newcellnb = self.roi.lineage.cells[-1]['id']
                                    
                                else:
                                    self.newcellnb = self.list_cellnbs_recycle.pop()
                                    self.roi.lineage.cells[self.newcellnb]['mother'] = self.mothercellnb
                                    self.roi.lineage.cells[self.newcellnb]['id'] = self.newcellnb
                                
                                for feature in self.features:
                                   # Remove incorrect information
                                   mothercell_previnfo = self.roi.lineage.cells[self.mothercellnb][feature][:index_frame_mothercell]
                                   mothercell_curinfo = self.roi.lineage.cells[self.mothercellnb][feature][index_frame_mothercell:]

                                   curcell_previnfo = self.roi.lineage.cells[self.curcellnb][feature][:index_frame_curcell]
                                   curcell_curinfo = self.roi.lineage.cells[self.curcellnb][feature][index_frame_curcell:]
                           
                                   self.roi.lineage.cells[self.mothercellnb][feature] = mothercell_previnfo                             
                                   self.roi.lineage.cells[self.curcellnb][feature] = curcell_previnfo                             
                                   
                                   # Add prevcell info to newcellnb
                                   self.roi.lineage.cells[self.newcellnb][feature].extend(mothercell_curinfo)
                                   # Switch info from curcellnb info to mothercellnb
                                   self.roi.lineage.cells[self.mothercellnb][feature].extend(curcell_curinfo)
                                    
                                self.roi.lineage.setvalue(self.mothercellnb,self.fnb,'daughters',self.newcellnb)                                  
                                    
                                self.cell_list_to_update.append(self.newcellnb)
    
                                for fnb in range(self.fnb,self.numframes):
                                    self.label_stack[fnb][self.label_stack[fnb] == self.mothercellnb + 1] = self.newcellnb + 1 
                                    self.label_stack[fnb][self.label_stack[fnb] == self.curcellnb + 1] = self.mothercellnb + 1
    
                                
                            # else just lineages need to be swapped and no new cell needs to be created
                            else:
    
                                # Swap lineages 
                                for feature in self.features:
                                    
                                    mothercell_previnfo = self.roi.lineage.cells[self.mothercellnb][feature][:index_frame_mothercell]
                                    mothercell_curinfo = self.roi.lineage.cells[self.mothercellnb][feature][index_frame_mothercell:]                                    
                                    
                                    curcellnb_previnfo = self.roi.lineage.cells[self.curcellnb][feature][:index_frame_curcell]
                                    curcellnb_curinfo = self.roi.lineage.cells[self.curcellnb][feature][index_frame_curcell:]

                                    self.roi.lineage.cells[self.mothercellnb][feature] = mothercell_previnfo
                                    self.roi.lineage.cells[self.curcellnb][feature] = curcellnb_previnfo
                                    
                                    self.roi.lineage.cells[self.curcellnb][feature].extend(mothercell_curinfo)
                                    self.roi.lineage.cells[self.mothercellnb][feature].extend(curcellnb_curinfo)
                                        
                                self.roi.lineage.cells[self.curcellnb]['mother'] = self.mothercellnb
                                
                                self.cell_list_to_update.append(self.curcellnb)
                                
                                for fnb in range(self.fnb,self.numframes):
                                    self.label_stack[fnb][self.label_stack[fnb] == self.mothercellnb + 1] = self.curcellnb + 1
                                    self.label_stack[fnb][self.label_stack[fnb] == self.curcellnb + 1] = self.mothercellnb + 1
                            
                        # Ensure the poles are correct orientation
                        self.roi.lineage.setvalue(self.mothercellnb,self.fnb,'old_pole',mother_poles[0])
                        self.roi.lineage.setvalue(self.mothercellnb,self.fnb,'new_pole',mother_poles[1])
                        
                        daucellnb = self.newcellnb if self.newcellnb else self.curcellnb
                        self.roi.lineage.setvalue(daucellnb,self.fnb,'old_pole',daughter_poles[0])
                        self.roi.lineage.setvalue(daucellnb,self.fnb,'new_pole',daughter_poles[1])
                        
                    #Else the cell of interest does not currently track to a cell
                    # Check poles and append lineage of new cell
                    else:
                       
                        if self.roi.lineage.cells[self.curcellnb]['daughters'][index_frame_curcell]:
                            daucellnb = self.roi.lineage.cells[self.curcellnb]['daughters'][index_frame_curcell]
                            self.roi.lineage.cells[self.curcellnb]['daughters'][index_frame_curcell] = None
                        else: 
                            daucellnb = None
                       
                        # Poles of mother/previous/old cell:
                        prev_old = self.roi.lineage.getvalue(self.mothercellnb, self.fnb - 1, "old_pole")
                        prev_new = self.roi.lineage.getvalue(self.mothercellnb, self.fnb - 1, "new_pole")                        
                       
                        cur_old_pole = self.roi.lineage.cells[self.curcellnb]['old_pole'][index_frame_curcell]
                        cur_new_pole = self.roi.lineage.cells[self.curcellnb]['new_pole'][index_frame_curcell]
    
                        # Find which pole is new vs old:
                        old_pole, new_pole = utils.track_poles((cur_old_pole,cur_new_pole), prev_old, prev_new)

                        self.roi.lineage.setvalue(self.curcellnb,self.fnb,'old_pole',old_pole)
                        self.roi.lineage.setvalue(self.curcellnb,self.fnb,'new_pole',new_pole)
                                

                        
                        for feature in self.features:                          
                            self.roi.lineage.cells[self.mothercellnb][feature].extend(self.roi.lineage.cells[self.curcellnb][feature][index_frame_curcell:])
                            
                            if daucellnb:
                                self.roi.lineage.cells[self.curcellnb][feature] = self.roi.lineage.cells[self.curcellnb][feature][:index_frame_curcell]
                                self.roi.lineage.cells[self.curcellnb][feature].extend(self.roi.lineage.cells[daucellnb][feature])
                                self.roi.lineage.cells[daucellnb][feature] = []
                            else:
                                self.roi.lineage.cells[self.curcellnb][feature] = [] if index_frame_curcell == 0 else self.roi.lineage.cells[self.curcellnb][feature][:index_frame_curcell]
                        
                        if daucellnb:
                            self.roi.lineage.cells[daucellnb]['id'] = -1
                            self.list_cellnbs_recycle.append(daucellnb)
                            
                        if self.roi.lineage.cells[self.curcellnb]['mother'] is not None:
                            mothernb =  self.roi.lineage.cells[self.curcellnb]['mother']
                            self.roi.lineage.cells[mothernb]['daughters'][self.fnb] = None

                        if index_frame_curcell == 0 and not daucellnb:
                            self.roi.lineage.cells[self.curcellnb]['id'] = -1
                            self.roi.lineage.cells[self.curcellnb]['mother'] = None
                            self.list_cellnbs_recycle.append(self.curcellnb)
                                
                        for fnb in range(self.fnb,self.numframes):
                            self.label_stack[fnb][self.label_stack[fnb] == self.curcellnb + 1] = self.mothercellnb + 1
                            
                            if daucellnb:
                                self.label_stack[fnb][self.label_stack[fnb] == daucellnb + 1] = self.curcellnb + 1                        
                        
                    # Go through lineage starting at self.fnb checking cells in cell_list_to_update
                    for frame in range(self.fnb + 1, self.numframes): # Do we need to analyze last frame??)
                        new_cell_list = []
                        # Analyze the lineage for one frame into the future
                        for cellnb in self.cell_list_to_update:
                            
                            frame_idx_mot = self.roi.lineage.cells[cellnb]['frames'].index(frame)
                            
                            # If cell divides, check of mother / daugher assignment is correct
                            if self.roi.lineage.cells[cellnb]['daughters'][frame_idx_mot]:
                                dau_cellnb = self.roi.lineage.cells[cellnb]['daughters'][frame_idx_mot]
                                new_cell_list.append(dau_cellnb)
                                # Identify mother and daughter poles:
                                mother_poles, daughter_poles, first_cell_is_mother = self.check_poles(cellnb,dau_cellnb,frame)     
                                                                    
                                # the mother and daughter cell are swapped    
                                if not first_cell_is_mother:
                                    
                                    # Save cell info because info will be deleted first then added
                                    mot_lin = self.get_pole_dau(cellnb,frame)
                                    dau_lin = self.get_pole_dau(dau_cellnb,frame)                  
                                    
                                    # probably don't need this line. frame_idx_dau should always be zero
                                    frame_idx_dau = self.roi.lineage.cells[dau_cellnb]['frames'].index(frame)
                                    
                                    if frame_idx_dau != 0:
                                        print(f'{frame_idx_dau} should be zero. This means the dau cell tracked to a previous cell')
    
                                    for feature in self.features:
                                        self.roi.lineage.cells[cellnb][feature] = self.roi.lineage.cells[cellnb][feature][:frame_idx_mot]
                                        self.roi.lineage.cells[dau_cellnb][feature] = self.roi.lineage.cells[dau_cellnb][feature][:frame_idx_dau]
                                        
                                        self.roi.lineage.cells[dau_cellnb][feature].extend(mot_lin[feature])
                                        self.roi.lineage.cells[cellnb][feature].extend(dau_lin[feature])
                                        
                                    for fnb in range(self.fnb,self.numframes):
                                        self.label_stack[fnb][self.label_stack[fnb] == cellnb + 1] = dau_cellnb + 1 
                                        self.label_stack[fnb][self.label_stack[fnb] == dau_cellnb + 1] = cellnb + 1  
                                          
                                # Ensure the poles are correct orientation
                                self.roi.lineage.setvalue(cellnb,frame,'old_pole',mother_poles[0])
                                self.roi.lineage.setvalue(cellnb,frame,'new_pole',mother_poles[1])
                                
                                self.roi.lineage.setvalue(dau_cellnb,frame,'old_pole',daughter_poles[0])
                                self.roi.lineage.setvalue(dau_cellnb,frame,'new_pole',daughter_poles[1])
                                    
                                self.roi.lineage.cells[dau_cellnb]['mother'] = cellnb
                                
                            else:
                                prev_old_pole = self.roi.lineage.getvalue(cellnb, frame - 1, "old_pole")
                                prev_new_pole = self.roi.lineage.getvalue(cellnb, frame - 1, "new_pole")
        
                                cur_old_pole = self.roi.lineage.getvalue(cellnb, frame, "old_pole")
                                cur_new_pole = self.roi.lineage.getvalue(cellnb, frame, "new_pole")
        
                                # Find which pole is new vs old:
                                old_pole, new_pole = utils.track_poles((cur_old_pole,cur_new_pole), prev_old_pole, prev_new_pole)
        
                                self.roi.lineage.setvalue(cellnb,frame,'old_pole',old_pole)
                                self.roi.lineage.setvalue(cellnb,frame,'new_pole',new_pole)
                                
                        if len(new_cell_list) > 0:
                            self.cell_list_to_update += new_cell_list
                                                
                    self.click_cell_prevframe=True
                    print('Click on a cell in the previous frame')
                    
                    # self.save()
        self.cell_list_to_update = []
        self.init_image()
        self.display_images()
        
    def key_press(self, event):
        '''
=        Parameters
        ----------
        The key n allows you to create a new cell with no previous lineage
        The key q allows you to exit
        -------
        None.

        '''
        key = event.char
        print(key)
        if key == 'n':    
            self.create_new_cell = True
            self.click_cell_prevframe = False
            self.init_image()
            self.display_images()
            
        elif key == 'q':
            self.create_new_cell = False
            self.click_cell_prevframe = True
            self.init_image()
            self.display_images()
            
        
    def get_pole_dau(self,cellnb,fnb):
        
        '''
        This function gets all information about the cell shown below
        frames, daughters, new_pole, old_pole, edges, length, width, area, perimeter
        '''
        
        idx_future_fnb_cell_exist = np.where(np.array(self.roi.lineage.cells[cellnb]['frames']) > fnb -1 )[0]
        idx_fnb_start = idx_future_fnb_cell_exist[0]
        idx_fnb_end = idx_future_fnb_cell_exist[-1] + 1
        
        cell_dict = {}
        
        for feature in self.features:
            cell_dict[feature] = self.roi.lineage.cells[cellnb][feature][idx_fnb_start:idx_fnb_end]

        return cell_dict
   
    def check_poles(self,cell_1,cell_2,fnb):
        '''
        This function determines which cell is the mother / daughter 
        and their respective old/new poles
        '''
        prev_old = self.roi.lineage.getvalue(cell_1, fnb - 1, "old_pole")
        prev_new = self.roi.lineage.getvalue(cell_1, fnb - 1, "new_pole")
        
        cell1_old = self.roi.lineage.getvalue(cell_1, fnb, "old_pole")
        cell1_new = self.roi.lineage.getvalue(cell_1, fnb, "new_pole")
        
        cell2_old = self.roi.lineage.getvalue(cell_2, fnb, "old_pole")
        cell2_new = self.roi.lineage.getvalue(cell_2, fnb, "new_pole")
        
            
        mother_poles, daughter_poles, first_cell_is_mother = utils.division_poles(
            [cell1_old,cell1_new], [cell2_old,cell2_new], prev_old, prev_new
                    )
        
        return mother_poles, daughter_poles, first_cell_is_mother
        
    def save(self):
        '''
        This function saves the results in the results folder. 
        It will also save the colored results if a color folder was specified.

        Returns
        -------
        None.

        '''
        
        cv2.imwrite(str(self.datapath / 'color' / (self.imgfps[self.img_nb_save-1].stem + '_t0.png')),self.GUI_colort0)
        cv2.imwrite(str(self.datapath / 'color' / (self.imgfps[self.img_nb_save-1].stem + '_t1.png')),self.GUI_colort1)
        cv2.imwrite(str(self.datapath / 'outputs' / self.imgfps[self.img_nb_save-1].name),self.outputs)
        
        print('save')
              
gui(
    imgpath = imgpath,
    pklpath = pklpath,
    )